//
//  Friends.m
//  myCar
//
//  Created by 吴东升 on 14/11/10.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "Friends.h"

@implementation Friends

- (void)dealloc
{
    [_idNum release];
    [_name release];
    [_proImage release];
    
    [super dealloc];
}

- (id)initWithDictionary:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

@end
