//
//  Friends.h
//  myCar
//
//  Created by 吴东升 on 14/11/10.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Friends : NSObject

@property (nonatomic, retain)NSString *idNum;
@property (nonatomic, retain)NSString *name;
@property (nonatomic, retain)NSString *proImage;

- (id)initWithDictionary:(NSDictionary *)dic;


@end
