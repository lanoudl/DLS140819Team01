//
//  MyFriendsViewController.m
//  myCar
//
//  Created by 吴东升 on 14/11/10.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "MyFriendsViewController.h"
#import "Friends.h"
#import "UIImageView+WebCache.h"
#import "FriendsCell.h"
#import "SinaViewController.h"

@interface MyFriendsViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
@property (nonatomic, retain)UITableView *myTableView;

@end

@implementation MyFriendsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.translucent = NO;
    [self initTableView];
}

- (void)initTableView
{
    self.myTableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    self.myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.myTableView.dataSource = self;
    self.myTableView.delegate = self;
    [self.view addSubview:self.myTableView];
    [_myTableView release];
    [self.myTableView registerClass:[FriendsCell class] forCellReuseIdentifier:@"1"];
    if (self.friArr.count == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"对不起" message:@"请登录" delegate:self cancelButtonTitle:@"返回" otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.friArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FriendsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"1"];
    Friends *fri = [self.friArr objectAtIndex:indexPath.row];
    cell.nameLabel.text = fri.name;
    [cell.myImage setImageWithURL:[NSURL URLWithString:fri.proImage]];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SinaViewController *sinaVC = [[SinaViewController alloc] init];
    Friends *fri = [self.friArr objectAtIndex:indexPath.row];
    sinaVC.idNum = fri.idNum;
    [self.navigationController pushViewController:sinaVC animated:YES];
    [sinaVC release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
