//
//  AboutCarViewController.m
//  myCar
//
//  Created by 吴东升 on 14/11/5.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "AboutCarViewController.h"
#import "WebColor.h"

@interface AboutCarViewController ()

@end

@implementation AboutCarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)setupViews
{
    self.view.backgroundColor = [WebColor whiteSmoke];
    
    UIImageView *logoImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, self.view.frame.size.width / 3, self.view.frame.size.height/ 5)];
    [logoImage setImage:[UIImage imageNamed:@"58.jpg"]];
    [self.view addSubview:logoImage];
    [logoImage release];
    
    UILabel *carLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 3 + 30, 30, 80, 30)];
    carLabel.text = @"爱尚汽车";
    carLabel.textColor = [UIColor redColor];
    [self.view addSubview:carLabel];
    [carLabel release];
    
    UILabel *logo = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 5 * 3 + 30, self.view.frame.size.height / 5, self.view.frame.size.width / 4, 15)];
    logo.text = @"Logo by 王晓宇";
    logo.textAlignment = 2;
    logo.font = [UIFont systemFontOfSize:10];
    [self.view addSubview:logo];
    [logo release];
    
    UILabel *editonLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 3 + 30, 70, 80, 30)];
    editonLabel.text = @"版本: v1.0";
    editonLabel.textColor = [UIColor redColor];
    [self.view addSubview:editonLabel];
    [editonLabel release];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, self.view.frame.size.height / 4, self.view.frame.size.width - 20, self.view.frame.size.height / 6)];
    label1.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:label1];
    [label1 release];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5,label1.frame.size.width / 4, label1.frame.size.height - 10)];
    imageView1.image = [UIImage imageNamed:@"liuli.jpg"];
    [label1 addSubview:imageView1];
    [imageView1 release];
    UILabel *nameLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(label1.frame.size.width / 4 + 10, 5, label1.frame.size.width / 4 * 3 - 20, label1.frame.size.height / 3 - 5)];
    nameLabel1.text = @"刘莉";
    [label1 addSubview:nameLabel1];
    [nameLabel1 release];
    UILabel *positonLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(label1.frame.size.width / 4 + 10, label1.frame.size.height / 3, label1.frame.size.width / 4 * 3 - 20, label1.frame.size.height / 3 - 5)];
    positonLabel1.text = @"ios开发工程师";
    positonLabel1.font = [UIFont systemFontOfSize:15];
    [label1 addSubview:positonLabel1];
    [positonLabel1 release];
    UILabel *emailLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(label1.frame.size.width / 4 + 10, label1.frame.size.height / 3 * 2, label1.frame.size.width / 4 * 3 - 20, label1.frame.size.height / 3 - 5)];
    emailLabel1.text = @"E-mail: 1053684907@qq.com";
    emailLabel1.font = [UIFont systemFontOfSize:12];
    [label1 addSubview:emailLabel1];
    [emailLabel1 release];
    
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, self.view.frame.size.height / 12 * 5 + 5 , self.view.frame.size.width - 20, self.view.frame.size.height / 6)];
    label2.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:label2];
    [label2 release];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5,label1.frame.size.width / 4, label2.frame.size.height - 10)];
    imageView2.image = [UIImage imageNamed:@"wzj.jpg"];
    [label2 addSubview:imageView2];
    [imageView2 release];
    UILabel *nameLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(label2.frame.size.width / 4 + 10, 5, label2.frame.size.width / 4 * 3 - 20, label2.frame.size.height / 3 - 5)];
    nameLabel2.text = @"王子洁";
    [label2 addSubview:nameLabel2];
    [nameLabel2 release];
    UILabel *positonLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(label2.frame.size.width / 4 + 10, label2.frame.size.height / 3, label2.frame.size.width / 4 * 3 - 20, label2.frame.size.height / 3 - 5)];
    positonLabel2.text = @"ios开发工程师";
    positonLabel2.font = [UIFont systemFontOfSize:15];
    [label2 addSubview:positonLabel2];
    [positonLabel2 release];
    UILabel *emailLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(label2.frame.size.width / 4 + 10, label2.frame.size.height / 3 * 2, label2.frame.size.width / 4 * 3 - 20, label2.frame.size.height / 3 - 5)];
    emailLabel2.text = @"E-mail: 1254147125@qq.com";
    emailLabel2.font = [UIFont systemFontOfSize:12];
    [label2 addSubview:emailLabel2];
    [emailLabel2 release];
    
    
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, self.view.frame.size.height / 12 * 7 + 10, self.view.frame.size.width - 20, self.view.frame.size.height / 6)];
    label3.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:label3];
    [label3 release];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5,label3.frame.size.width / 4, label3.frame.size.height - 10)];
    imageView3.image = [UIImage imageNamed:@"0326.jpg"];
    [label3 addSubview:imageView3];
    [imageView3 release];
    UILabel *nameLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(label3.frame.size.width / 4 + 10, 5, label3.frame.size.width / 4 * 3 - 20, label3.frame.size.height / 3 - 5)];
    nameLabel3.text = @"吴东升";
    [label3 addSubview:nameLabel3];
    [nameLabel3 release];
    UILabel *positonLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(label3.frame.size.width / 4 + 10, label3.frame.size.height / 3, label3.frame.size.width / 4 * 3 - 20, label3.frame.size.height / 3 - 5)];
    positonLabel3.text = @"ios开发工程师";
    positonLabel3.font = [UIFont systemFontOfSize:15];
    [label3 addSubview:positonLabel3];
    [positonLabel3 release];
    UILabel *emailLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(label3.frame.size.width / 4 + 10, label3.frame.size.height / 3 * 2, label3.frame.size.width / 4 * 3 - 20, label3.frame.size.height / 3 - 5)];
    emailLabel3.text = @"E-mail: wu_dongsheng@yahoo.com";
    emailLabel3.font = [UIFont systemFontOfSize:12];
    [label3 addSubview:emailLabel3];
    [emailLabel3 release];
    
    
    
}

- (void)dealloc
{
    [super dealloc];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    [self setupViews];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
