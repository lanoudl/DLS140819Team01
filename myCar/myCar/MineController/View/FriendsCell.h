//
//  FriendsCell.h
//  myCar
//
//  Created by 吴东升 on 14/11/10.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsCell : UITableViewCell

@property (nonatomic, retain)UIImageView *myImage;
@property (nonatomic, retain)UILabel *nameLabel;

@end
