//
//  FriendsCell.m
//  myCar
//
//  Created by 吴东升 on 14/11/10.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "FriendsCell.h"

@implementation FriendsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.myImage = [[UIImageView alloc] init];
        [self.contentView addSubview:self.myImage];
        
        self.nameLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.nameLabel];
    }
    return self;
}

- (void)dealloc
{
    [_myImage release];
    [_nameLabel release];
    
    [super dealloc];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.myImage.frame = CGRectMake(0, 0, self.contentView.frame.size.width / 5, self.contentView.frame.size.height);
    self.nameLabel.frame = CGRectMake(self.contentView.frame.size.width / 5 + 10, 0, self.contentView.frame.size.width / 5 * 3, self.contentView.frame.size.height);
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
