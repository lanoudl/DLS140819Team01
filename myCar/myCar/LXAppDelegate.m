//
//  LXAppDelegate.m
//  myCar
//
//  Created by 刘莉 on 14-10-27.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "LXAppDelegate.h"
#import "InformationViewController.h"
#import "ForumViewController.h"
#import "MineViewController.h"
#import "SeekCarViewController.h"
#import "MineCenterViewController.h"
#import "UIImageView+WebCache.h"
#import <ShareSDK/ShareSDK.h>
#import "WeiboApi.h"  // 腾讯
#import "WeiboSDK.h"  // 新浪


@implementation LXAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [NSThread sleepForTimeInterval:2];
    [ShareSDK registerApp:@"api12"];
    //添加新浪微博应用
    [ShareSDK connectSinaWeiboWithAppKey:@"2874072217"
                               appSecret:@"09465a1f32dc6de01d76de3115ce7d8e"
                             redirectUri:@"http://www.sharesdk.cn"];
    //添加腾讯微博应用
    [ShareSDK connectTencentWeiboWithAppKey:@"801307650"
                                  appSecret:@"ae36f4ee3946e1cbb98d6965b0b2ff5c"
                                redirectUri:@"http://www.sharesdk.cn"
                                   wbApiCls:[WeiboApi class]];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    //网络变化判断
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.hostReach = [[Reachability reachabilityWithHostName:@"www.baidu.com"]retain];
    [self.hostReach startNotifier];
    
    [self.window makeKeyAndVisible];
    
    InformationViewController *infoVC = [[InformationViewController alloc]init];
    infoVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"首页" image:[UIImage imageNamed:@"zixun.png"] tag:1000];
    UINavigationController *firstVC = [[UINavigationController alloc]initWithRootViewController:infoVC];
    
    
    ForumViewController *forumVC = [[ForumViewController alloc]init];
    forumVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"论坛" image: [UIImage imageNamed:@"luntan.png"] tag:1001];
#warning 内存问题
    [forumVC.tabBarItem release];
    UINavigationController *secondVC = [[ UINavigationController alloc]initWithRootViewController:forumVC];
    SeekCarViewController *seekVC =[[ SeekCarViewController alloc]init];
    seekVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"找车" image:[UIImage imageNamed:@"qiche.png"] tag:1002];
    [seekVC.tabBarItem release];
    UINavigationController *thirdVC = [[UINavigationController alloc]initWithRootViewController:seekVC];
    UITabBarController *tabVC =[[ UITabBarController alloc]init];
    MineCenterViewController *mineVC = [[MineCenterViewController alloc]init];
    mineVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"我的" image:[UIImage imageNamed:@"wode.png"] tag:1003];
    UINavigationController *forthVC = [[UINavigationController alloc]initWithRootViewController:mineVC];
    tabVC.viewControllers = @[firstVC, secondVC, thirdVC, forthVC];
    self.window.rootViewController = tabVC;
    
    [infoVC   release];
    [mineVC   release];
    [seekVC   release];
    [forumVC  release];
    [firstVC  release];
    [secondVC release];
    [thirdVC  release];
    [forthVC  release];
    [tabVC    release];
    [_window  release];
    return YES;
}


- (void)dealloc
{
    [_window release];
    [super   dealloc];
}

//判断连接改变
- (void)reachabilityChanged:(NSNotification *)note
{
    Reachability *preachability = [note object];
    NSParameterAssert([preachability isKindOfClass:[Reachability class]]);
    //NSString *pStr_3G =@"当前网络为2G或3G,请注意流量";
   // NSString *pStr_WiFi =@"当前网络为Wi-Fi,请放心使用 ";
    //NSString *pStr_WLAN =@"当前网络为WLAN";
    NSString *pStr_NO = @"当前网络已经断开";
    switch ([preachability currentReachabilityStatus]) {
        case NotReachable:
            [self alertShow:pStr_NO];
            break;
        case ReachableViaWiFi:
           // [self alertShow:pStr_WiFi];
            break;
        case ReachableViaWWAN:
           // [self alertShow:pStr_3G];
            break;
        default:
            break;
    }
}
//提示框
- (void)alertShow:(NSString *)mes
{
    UIAlertView *AlertView = [[UIAlertView alloc]initWithTitle:@"通知" message:mes delegate:nil cancelButtonTitle:@"确认" otherButtonTitles: nil];
    [AlertView show];
    [AlertView release];
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
