//
//  HttpViewController.m
//  myCar
//
//  Created by 刘莉 on 14-11-6.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "HttpViewController.h"
#import <Comment/Comment.h>
#import "SaveNews.h"
#import "DataBaseHandler.h"
#import "WebColor.h"
#import "MBProgressHUD.h"

@interface HttpViewController ()<UIWebViewDelegate, UIWebViewDelegate>
@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, retain) UIButton *collectButton;
@property (nonatomic, retain) UIButton  *backButton;
@property (nonatomic, retain) UIButton  *forwardButton;
@property (nonatomic, retain) MBProgressHUD *hub;


@end

@implementation HttpViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.news = [[SaveNews alloc] init];
        self.car = [[ExaxtCar alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [_collectButton release];
    [_backButton release];
    [_forwardButton release];
    [_news release];
    [_car release];
    _webView.delegate = nil;
    [_webView release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self topView];
    [self footView];
    [self setWebView];
    self.news.title = self.car.serialName;
    self.news.image = self.car.photo;
    self.news.url = self.car.idNum;
    
    // 菊花转起来
    [self setHub];
}

- (void)setWebView
{
    self.webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, -52, self.view.bounds.size.width, self.view.bounds.size.height + 49 + 52)];
    self.webView.delegate = self;
    
    if (self.car.idNum == nil) {
        self.car.idNum = self.news.url;
    }
    NSString *str = [NSString stringWithFormat:@"http://price.pcauto.com.cn/sg%@/",self.car.idNum];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:str] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60];
    [self.webView  loadRequest:request];
    self.webView.scrollView.bounces = NO;
    
    [self.view addSubview:self.webView];
    [self.view sendSubviewToBack:self.webView];
}

- (void)setHub
{
    self.hub = [[MBProgressHUD alloc] initWithView:self.view];
    [self.hub setMode:MBProgressHUDModeIndeterminate];
    [self.hub setFrame:CGRectMake(0, 0, 320, 480)];
    [self.hub setMinSize:CGSizeMake(100, 100)];
    [self.view addSubview:self.hub];
    [self.hub show:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    self.tabBarController.tabBar.hidden = YES;
}


- (void)topView
{
    UIView *topWhiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 20)];
    topWhiteView.backgroundColor = [UIColor whiteColor];
    //self.navigationController.navigationBarHidden = YES;
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 64)];
    [topView addSubview:topWhiteView];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [backButton setFrame:CGRectMake(0, 20, self.view.frame.size.width / 4, 44)];
    topView.backgroundColor = [WebColor dodgerBlue];
    [backButton setImage:[UIImage imageNamed:@"fanhui.png"] forState:(UIControlStateNormal)];
    backButton.tintColor = [WebColor white];
    [backButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    [topView addSubview:backButton];
    self.collectButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.collectButton.frame = CGRectMake(self.view.frame.size.width - 100, 30, 30, 25);
    self.collectButton.tintColor = [WebColor white];
    DataBaseHandler *handler = [DataBaseHandler shareInstance];
    [handler createTable];
    NSArray *arr = [handler selectAllCar];
    int flag = 1;
    for (SaveNews *car in arr) {
        NSString *str = [NSString stringWithFormat:@"%@", self.news.url];
        if ([car.url isEqualToString:str]) {
            [self.collectButton setImage:[UIImage imageNamed:@"collect2.png"] forState:UIControlStateNormal];
            flag = 0;
            break;
        }
    }
    if (flag == 1) {
        [self.collectButton setImage:[UIImage imageNamed:@"collect1.png"] forState:UIControlStateNormal];
    }
    [self.collectButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:self.collectButton];
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [shareButton setImage:[UIImage imageNamed:@"share.png"] forState:(UIControlStateNormal)];
    shareButton.tintColor = [UIColor whiteColor];
    shareButton.frame = CGRectMake(self.view.frame.size.width - 40, 20, 30, 44);
    [topView addSubview:shareButton];
    [shareButton addTarget:self action:@selector(share:) forControlEvents:(UIControlEventTouchUpInside)];
    
    [topView addSubview:self.collectButton];
    [self.view addSubview:topView];
    [self.view bringSubviewToFront:topView];
    [topWhiteView release];
    [topView release];
    
}
- (void)backButtonClicked:(UIButton *)button
{
    //self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = NO;
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)footView
{
    UIView *tabView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - 49, self.view.bounds.size.width, 49)];
    tabView.backgroundColor = [WebColor whiteSmoke];
    [self.view addSubview:tabView];
    self.backButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.backButton setImage:[UIImage imageNamed:@"houtui.png"] forState:(UIControlStateNormal)];
    [self.backButton setFrame:CGRectMake(0, 0, self.view.frame.size.width / 4, 49)];
    [tabView addSubview:self.backButton];
    [self.backButton addTarget:self action:@selector(returnButtonClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    self.forwardButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.forwardButton setFrame:CGRectMake(self.view.frame.size.width / 4 * 3, 0, self.view.frame.size.width / 4, 49)];
    [self.forwardButton setImage:[UIImage imageNamed:@"shuaxin.png"] forState:(UIControlStateNormal)];
    [self.forwardButton addTarget:self action:@selector(refreshAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [tabView addSubview:self.forwardButton];
    
    [self.view bringSubviewToFront:tabView];
    [tabView release];
    
}
- (void)returnButtonClicked:(UIButton *)button
{
    [self.webView goBack];
}

- (void)refreshAction:(UIButton *)button
{
    [self.webView reload];
    
}
- (void)buttonClicked:(UIButton *)button
{
    DataBaseHandler *handler = [DataBaseHandler shareInstance];
    if (button.currentImage == nil || ([button.currentImage isEqual:[UIImage imageNamed:@"collect1.png"]])) {
        [button setImage:[UIImage imageNamed:@"collect2.png"] forState:UIControlStateNormal];
        [handler insertCar:self.news];
    } else {
        [button setImage:[UIImage imageNamed:@"collect1.png"] forState:UIControlStateNormal];
        [handler deleteCar:self.news];
    }
}

- (void)share:(UIButton *)button
{
    id<ISSContent> content = [ShareSDK content: self.car.serialId defaultContent:@"要分享的内容为空时" image:nil title:@"share" url:@"http://www.baidu.com" description:@"这是描述" mediaType:SSPublishContentMediaTypeNews];
    [ShareSDK showShareActionSheet:nil shareList:nil content:content statusBarTips:YES authOptions:nil shareOptions:nil result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享" message:nil delegate:self cancelButtonTitle:@"返回" otherButtonTitles:nil, nil];
        [self.view addSubview:alertView];
        if (state == SSResponseStateSuccess) {
            NSLog(@"分享成功");
            [alertView setTitle:@"分享成功"];
            [alertView show];
        } else if (state == SSResponseStateFail) {
            NSLog(@"分享失败");
            [alertView setTitle:@"分享失败"];
            [alertView show];
        }
    }];
    
}

#pragma mark
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.forwardButton setImage:[UIImage imageNamed:@"chahao.png"] forState:(UIControlStateNormal)];

}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.forwardButton setImage:[UIImage imageNamed:@"shuaxin.png"] forState:(UIControlStateNormal)];
    [self.hub removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
