//
//  ExactCarController.m
//  myCar
//
//  Created by 刘莉 on 14-11-1.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "ExactCarController.h"
#import "ExaxtCar.h"
#import "CarValueCell.h"
#import "HttpViewController.h"
@interface ExactCarController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, retain) NSMutableArray *carArr; // 车数组
@end

@implementation ExactCarController

- (void)dealloc
{
    [_arr    release];
    [_carArr release];
    [super   dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.carArr = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"筛选结果";
    for (NSDictionary *dic in self.arr) {
        ExaxtCar *car = [[ExaxtCar alloc]initWithDictionary:dic];
        [self.carArr addObject:car];
        [car release];
    }
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 64) style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [tableView registerClass:[CarValueCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:tableView];
    [tableView release];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.carArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CarValueCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    ExaxtCar *car = self.carArr[indexPath.row];
    NSLog(@"nihao%@", car.idNum);
    cell.car = car;
    return cell;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ExaxtCar *car = self.carArr[indexPath.row];
    car.idNum  = car.serialId;
    HttpViewController *httpVC = [[HttpViewController alloc]init];
    httpVC.car =  car;
    
    [self.navigationController pushViewController:httpVC animated:YES];
    [httpVC release];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
