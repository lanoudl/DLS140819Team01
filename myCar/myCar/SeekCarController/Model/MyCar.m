//
//  MyCar.m
//  myCar
//
//  Created by 刘莉 on 14-10-29.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "MyCar.h"

@implementation MyCar

- (void)dealloc
{
    [_count release];
    [_name  release];
    [_value release];
    [super  dealloc];
}
- (id)initWithDictionary:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}
@end
