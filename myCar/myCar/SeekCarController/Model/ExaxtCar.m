//
//  ExaxtCar.m
//  myCar
//
//  Created by 刘莉 on 14-11-1.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "ExaxtCar.h"

@implementation ExaxtCar

- (void)dealloc
{
    [_idNum release];
    [_photo      release];
    [_priceRange release];
    [_serialId   release];
    [_serialName release];
    [_isNew      release];
    [super       dealloc];
}
- (id)initWithDictionary:(NSDictionary *)dic
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}
- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.idNum = value;
    }
    if ([key isEqualToString:@"name"]) {
        self.serialName = value;
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.idNum = [aDecoder decodeObjectForKey:@"idNum"];
        self.serialId = [aDecoder decodeObjectForKey:@"serialId"];
        self.serialName = [aDecoder decodeObjectForKey:@"serialName"];
        
        
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    //  编码的方法，将对象的所有数据存在一起,方便存储
    
    // 将某一条数据编码
    [aCoder encodeObject :self.idNum  forKey:@"idNum"];
    [aCoder encodeObject :self.serialId   forKey:@"serialId"];
    [aCoder encodeObject :self.serialName    forKey:@"serialName"];
    
}
@end
