//
//  MyCar.h
//  myCar
//
//  Created by 刘莉 on 14-10-29.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyCar : NSObject
@property (nonatomic, retain)NSString *name;
@property (nonatomic, retain)NSString *value;
@property (nonatomic, retain)NSString *count;
- (id)initWithDictionary:(NSDictionary *)dic;
@end
