//
//  ExaxtCar.h
//  myCar
//
//  Created by 刘莉 on 14-11-1.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExaxtCar : NSObject<NSCoding>
@property (nonatomic, retain) NSString *photo;
@property (nonatomic, retain) NSString *priceRange;
@property (nonatomic, retain) NSString *serialId;
@property (nonatomic, retain) NSString *serialName;
@property (nonatomic, retain) NSNumber *isNew;
@property (nonatomic, retain) NSString *idNum;


- (id)initWithDictionary:(NSDictionary *)dic;

@end
