//
//  HotCell.m
//  myCar
//
//  Created by 刘莉 on 14-11-1.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "HotCell.h"

@implementation HotCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.hotImgView = [[UIImageView alloc]init];
        [self.contentView addSubview:self.hotImgView];
        self.hotLabel = [[UILabel alloc]init];
        [self.contentView addSubview:self.hotLabel];
        self.nameLabel = [[UILabel alloc]init];
        [self.contentView addSubview:self.nameLabel];
        [_nameLabel  release];
        [_hotLabel   release];
        [_hotImgView release];
    }
    return self;
}

- (void)dealloc
{
    [_nameLabel  release];
    [_hotLabel   release];
    [_hotImgView release];
    [super dealloc];
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat width = self.contentView.frame.size.width;
    CGFloat height = self.contentView.frame.size.height;
    self.hotImgView.frame = CGRectMake(0, 0, width / 4, height);
    self.hotLabel.frame = CGRectMake(width / 4, 0, width / 4 * 3, height / 2);
    self.nameLabel.frame = CGRectMake(width / 4, height / 2, width / 4 * 3, height / 2);
    self.nameLabel.font = [UIFont systemFontOfSize:12];
    
}
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
