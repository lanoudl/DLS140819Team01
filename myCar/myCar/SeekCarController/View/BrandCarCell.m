//
//  BrandCarCell.m
//  myCar
//
//  Created by 刘莉 on 14-10-28.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "BrandCarCell.h"
#import "UIImageView+WebCache.h"
@implementation BrandCarCell

- (void)dealloc
{
    [_nameLabel   release];
    [_carLogoView release];
    [_car release];
    [super dealloc];
    
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _carLogoView = [[UIImageView alloc]init];
        [self.contentView addSubview:self.carLogoView];
        _nameLabel = [[UILabel alloc]init];
        [self.contentView addSubview:self.nameLabel];
        self.nameLabel.textAlignment = NSTextAlignmentLeft;
        
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    self.carLogoView.frame = CGRectMake(20, 20, 60, 40);
    self.nameLabel.frame = (CGRectMake(100, 5, self.frame.size.width - 80, self.frame.size.height));
    
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setCar:(Car *)car
{
    if (_car != car) {
        [_car release];
        _car = [car retain];
    }
    self.nameLabel.text = _car.name;
    [self.carLogoView setImageWithURL:[NSURL URLWithString:_car.logo]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
