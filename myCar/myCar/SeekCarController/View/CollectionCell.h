//
//  CollectionCell.h
//  myCar
//
//  Created by 刘莉 on 14-10-30.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionCell : UICollectionViewCell
@property (nonatomic, retain)UILabel *cellLabel;
@end
