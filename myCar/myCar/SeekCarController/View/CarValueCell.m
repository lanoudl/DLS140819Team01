//
//  CarValueCell.m
//  myCar
//
//  Created by 刘莉 on 14-11-1.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "CarValueCell.h"
#import "UIImageView+WebCache.h"
@implementation CarValueCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _logoView = [[UIImageView alloc]init];
        [self.contentView addSubview:_logoView];
        _nameLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_nameLabel];
        _newLabel = [[UILabel alloc]init];
    
        _newLabel.textColor = [UIColor whiteColor];
        _newLabel.font = [UIFont systemFontOfSize:14];
        
        [self.contentView addSubview:_newLabel];
        _valueLabel  = [[UILabel alloc]init];
        _valueLabel.font = [UIFont systemFontOfSize:14];
        _valueLabel.textColor = [UIColor redColor];
        [self.contentView addSubview:_valueLabel];
        
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}
- (void)dealloc
{
    [_logoView   release];
    [_nameLabel  release];
    [_valueLabel release];
    [_car        release];
    [super       dealloc];
}
- (void)setCar:(ExaxtCar *)car
{
    if (_car != car) {
        [_car release];
        _car = [car retain];
    }
    
    [self.logoView setImageWithURL:[NSURL URLWithString:_car.photo]];
    self.nameLabel.text  = _car.serialName;
    if ([_car.priceRange isEqualToString:@"null"]) {
        _car.priceRange = @"没有报价";
    }
    self.valueLabel.text = _car.priceRange;
    self.newLabel.backgroundColor = [UIColor clearColor];
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat width = self.contentView.frame.size.width;
    CGFloat height = self.contentView.frame.size.height;
    self.logoView.frame = CGRectMake(20, 10, 80, height - 20);
    self.nameLabel.frame = CGRectMake(110, 10, width  - 100, height / 2 - 10);
    self.valueLabel.frame = CGRectMake(110, height / 2 , width  - 100, height / 2 - 10);
    self.newLabel.frame = CGRectMake(18, 8, 60, 15);
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
