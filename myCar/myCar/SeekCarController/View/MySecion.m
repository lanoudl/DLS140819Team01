//
//  MySecion.m
//  myCar
//
//  Created by 刘莉 on 14-10-30.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "MySecion.h"

@implementation MySecion

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.secionName = [[UILabel alloc]initWithFrame:frame];
        [self addSubview:self.secionName];
        [_secionName release];
    }
    return self;
}

- (void)dealloc
{
    [_secionName release];
    [super dealloc];
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    self.secionName.frame = self.bounds;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
