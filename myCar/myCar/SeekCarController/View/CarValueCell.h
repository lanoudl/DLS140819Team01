//
//  CarValueCell.h
//  myCar
//
//  Created by 刘莉 on 14-11-1.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExaxtCar.h"
@interface CarValueCell : UITableViewCell
@property (nonatomic, retain) UIImageView *logoView;// 车的图片
@property (nonatomic, retain) UILabel     *nameLabel;// 车的名字
@property (nonatomic, retain) UILabel     *valueLabel;// 报价
@property (nonatomic, retain) UILabel     *newLabel;  // 新车标志
@property (nonatomic, retain) ExaxtCar    *car;


@end
