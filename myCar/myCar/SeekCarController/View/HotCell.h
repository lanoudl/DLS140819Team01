//
//  HotCell.h
//  myCar
//
//  Created by 刘莉 on 14-11-1.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotCell : UITableViewCell
@property (nonatomic, retain)UIImageView *hotImgView;
@property (nonatomic, retain)UILabel  *hotLabel;
@property (nonatomic, retain)UILabel  *nameLabel;
@end
