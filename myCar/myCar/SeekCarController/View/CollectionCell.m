//
//  CollectionCell.m
//  myCar
//
//  Created by 刘莉 on 14-10-30.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "CollectionCell.h"

@implementation CollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.cellLabel = [[UILabel alloc]init];
        self.cellLabel.userInteractionEnabled = YES;
        self.cellLabel.textAlignment = NSTextAlignmentCenter;
        self.cellLabel.backgroundColor = [UIColor whiteColor];
        
        self.cellLabel.numberOfLines = 0;
        [self addSubview:self.cellLabel];
        [_cellLabel release];
    }
    return self;
}
- (void)dealloc
{
    [_cellLabel release];
    [super dealloc];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.cellLabel.frame = self.bounds;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
