//
//  BrandCarCell.h
//  myCar
//
//  Created by 刘莉 on 14-10-28.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Car.h"
@interface BrandCarCell : UITableViewCell
@property (nonatomic, retain)  UIImageView *carLogoView;
@property (nonatomic, retain)  UILabel  *nameLabel;
@property (nonatomic, retain)  Car *car;
@end
