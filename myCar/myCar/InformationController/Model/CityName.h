//
//  CityName.h
//  myCar
//
//  Created by 王子洁 on 14/11/1.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityName : NSObject

@property (nonatomic, retain)NSString *pro;
@property (nonatomic, retain)NSString *city;
@property (nonatomic, retain)NSString *index;
@end
