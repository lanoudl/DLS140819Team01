//
//  Information.h
//  myCar
//
//  Created by 刘莉 on 14-10-27.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Information : NSObject

@property (nonatomic, retain)NSString *title;
@property (nonatomic, retain)NSString *image;
@property (nonatomic, retain)NSString *number;
@property (nonatomic, retain)NSString *count;
@property (nonatomic, retain)NSString *date;
@property (nonatomic, retain)NSString *url;
@property (nonatomic, retain)NSString *content;

- (id)initWithArray:(NSArray *)arr;
@end
