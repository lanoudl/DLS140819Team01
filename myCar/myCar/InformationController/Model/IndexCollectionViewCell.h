//
//  IndexCollectionViewCell.h
//  myCar
//
//  Created by 王子洁 on 14/10/28.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Information.h"
@interface IndexCollectionViewCell : UICollectionViewCell

@property (nonatomic, retain)UIImageView *myImageView;

@property (nonatomic, retain)UILabel *myLabel;
@property (nonatomic, retain)Information *info;
@end
