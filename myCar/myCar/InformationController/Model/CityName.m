//
//  CityName.m
//  myCar
//
//  Created by 王子洁 on 14/11/1.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "CityName.h"

@implementation CityName

- (void)dealloc
{
    [_city release];
    [_pro release];
    [_index release];
    [super dealloc];
}

@end
