//
//  IndexCollectionViewCell.m
//  myCar
//
//  Created by 王子洁 on 14/10/28.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "IndexCollectionViewCell.h"

@implementation IndexCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.myImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:self.myImageView];
        [_myImageView release];
        
        self.myLabel = [[UILabel alloc] init];
       // self.myLabel.backgroundColor = [UIColor greenColor];
        [self.contentView addSubview:self.myLabel];
        [_myLabel release];
        
    }
    return self;
}

- (void)setInfo:(Information *)info
{
    if (_info != info) {
        [_info release];
        _info = [info retain];
    }
    if ([self.info.title isEqualToString:@"首页"]) {
        self.info.title = @"资讯";
    }
    self.myLabel.text = _info.title;
    
    
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    self.myImageView.frame = CGRectMake(30, 20, 40, 40);
    self.myLabel.frame = CGRectMake(32, 55, 60, 50);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
