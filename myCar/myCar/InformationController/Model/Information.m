//
//  Information.m
//  myCar
//
//  Created by 刘莉 on 14-10-27.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "Information.h"

@implementation Information

- (void)dealloc
{
    [_image release];
    [_title release];
    [_number release];
    [_date release];
    [_count release];
    [_url release];
    [_content release];
    [super dealloc];
}
- (id)initWithArray:(NSArray *)arr
{
    self = [super init];
    self.number = [arr objectAtIndex:0];
    self.title = [arr objectAtIndex:1];
    if ([self.title isEqualToString:@"首页"]) {
        self.title = @"资讯";
    }
    self.title  = [arr objectAtIndex:1];
    return self;
}
@end
