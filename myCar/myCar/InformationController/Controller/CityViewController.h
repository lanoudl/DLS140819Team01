//
//  CityViewController.h
//  myCar
//
//  Created by 王子洁 on 14/11/4.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityName.h"
@protocol CityViewControllerDelegate <NSObject>

- (void)passValue:(CityName *)cityName;

@end

@interface CityViewController : UIViewController

@property (nonatomic, assign)id<CityViewControllerDelegate>delegate;

@end
