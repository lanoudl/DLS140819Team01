//
//  DataBaseHandler.h
//  myCar
//
//  Created by 王子洁 on 14/11/5.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "SaveNews.h"
#import "Base.h"
@interface DataBaseHandler : NSObject
{
    sqlite3 *dbPoint;
}

+ (DataBaseHandler *)shareInstance;

- (void)openDB;

- (void)insertNews:(SaveNews *)news;
///  创建收藏 table
- (void)createCollectionTable;
// 添加论坛收藏  注释：不是帖子
- (void)insertCollection:(Base *)base;
// 删除论坛收藏
- (void)deleteCollection:(Base *)base;
/// 返回所有的论坛收藏
- (NSArray *)selectCollectionAll;
- (void)deleteNews:(SaveNews *)news;

- (void)createTable;

- (void)createForumTable;

- (void)createCarTable;

- (void)insertForum:(SaveNews *)forum;

- (void)deleteForum:(SaveNews *)forum;

- (void)insertCar:(SaveNews *)car;

- (void)deleteCar:(SaveNews *)car;

- (NSMutableArray *)selectAllCar;

- (NSMutableArray *)selectAll;

- (NSMutableArray *)selectAllForum;

@end
