//
//  WebViewController.m
//  myCar
//
//  Created by 王子洁 on 14/10/29.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "WebViewController.h"
#import <Comment/Comment.h>
#import "SaveNews.h"
#import "DataBaseHandler.h"
#import "MBProgressHUD.h"

#import "WebColor.h"
@interface WebViewController ()<UIGestureRecognizerDelegate, UIWebViewDelegate>

@property (nonatomic, retain)UIButton *collectButton;

@property (nonatomic, retain)DataBaseHandler *handler;

@property (nonatomic, retain)MBProgressHUD *hub;

@end


@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
       self.news = [[SaveNews alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [_news release];
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.backgroundColor = [WebColor dodgerBlue];
    self.navigationController.navigationBar.tintColor = [WebColor white];
    [self setWebView];
    self.handler = [DataBaseHandler shareInstance];
    [self.handler openDB];
    [self.handler createTable];
    
    if (self.info.url != nil) {
        self.news.url = self.info.url;
        self.news.image = self.info.image;
        self.news.title = self.info.title;
    }
    [self setButton];

    [self.navigationController.navigationBar addSubview:self.collectButton];
   
    [self setHub];
    // Do any additional setup after loading the view.
}

- (void)setWebView
{
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, -140, self.view.bounds.size.width, self.view.bounds.size.height + 140 + 64 + 50)];
    webView.delegate = self;
    webView.scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    if (self.news.url != nil) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.news.url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60];
        [webView loadRequest:request];
    } else {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.info.url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60];
        [webView loadRequest:request];
    }
    webView.scrollView.bounces = NO;
    [self.view addSubview:webView];
    [self setNavItem];
    [webView release];
}

- (void)setHub
{
    self.hub = [[MBProgressHUD alloc] initWithView:self.view];
    [self.hub setMode:MBProgressHUDModeIndeterminate];
    [self.hub setFrame:CGRectMake(0, 0, 320, 480)];
    [self.hub setMinSize:CGSizeMake(100, 100)];
    [self.view addSubview:self.hub];
    [self.hub show:YES];
}

- (void)setNavItem
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"share.png"] style:UIBarButtonItemStylePlain target:self action:@selector(share:)];
   self.navigationItem.rightBarButtonItem.tintColor = [WebColor white];
}

- (void)setButton
{
    self.collectButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.collectButton.frame = CGRectMake(self.view.frame.size.width - 100, 8, 30, 25);
    [self.collectButton setImage:[UIImage imageNamed:@"collect1.png"] forState:UIControlStateNormal];
    
    [self.collectButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    NSMutableArray *arr = [self.handler selectAll];
    for (SaveNews *news in arr) {
        
        // NSLog(@"%d.....", a);
        if ([news.url isEqualToString:self.news.url]) {
            [self.collectButton setImage:[UIImage imageNamed:@"collect2.png"] forState:UIControlStateNormal];
            break;
        } else {
            [self.collectButton setImage:[UIImage imageNamed:@"collect1.png"] forState:UIControlStateNormal];
            //NSLog(@"-------");
        }
        
    }
}

- (void)buttonClicked:(UIButton *)button
{
    if (button.currentImage == nil || ([button.currentImage isEqual:[UIImage imageNamed:@"collect1.png"]])) {
        [button setImage:[UIImage imageNamed:@"collect2.png"] forState:UIControlStateNormal];
        [self.handler insertNews:self.news];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"收藏成功" delegate:self cancelButtonTitle:@"请返回" otherButtonTitles:nil, nil];
        [alertView show];

    } else {
        [button setImage:[UIImage imageNamed:@"collect1.png"] forState:UIControlStateNormal];
        [self.handler deleteNews:self.news];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"删除成功" delegate:self cancelButtonTitle:@"请返回" otherButtonTitles:nil, nil];
        [alertView show];
    }
}


- (void)share:(UIBarButtonItem *)button
{
    id<ISSContent> content = [ShareSDK content: self.info.url defaultContent:@"要分享的内容为空时" image:nil title:@"share" url:@"http://www.baidu.com" description:@"这是描述" mediaType:SSPublishContentMediaTypeNews];
    
    [ShareSDK showShareActionSheet:nil shareList:nil content:content statusBarTips:YES authOptions:nil shareOptions:nil result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享" message:nil delegate:self cancelButtonTitle:@"返回" otherButtonTitles:nil, nil];
        [self.view addSubview:alertView];
        if (state == SSResponseStateSuccess) {
            NSLog(@"分享成功");
            [alertView setTitle:@"分享成功"];
            [alertView show];
        } else if (state == SSResponseStateFail) {
            NSLog(@"分享失败");
            [alertView setTitle:@"分享失败"];
            [alertView show];
        }
    }];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    self.collectButton.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.collectButton.hidden = YES;

}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.hub removeFromSuperview];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
