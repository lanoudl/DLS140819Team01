//
//  WebViewController.h
//  myCar
//
//  Created by 王子洁 on 14/10/29.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Information.h"
#import "SaveNews.h"
@interface WebViewController : UIViewController

@property (nonatomic, retain)Information *info;

@property (nonatomic, retain)SaveNews *news;

@end
