//
//  CollectTableViewCell.m
//  myCar
//
//  Created by 王子洁 on 14/11/6.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "CollectTableViewCell.h"

@implementation CollectTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.collectImage = [[UIImageView alloc] init];
        [self.contentView addSubview:self.collectImage];
        self.collectLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.collectLabel];
        
    }
    return self;
}

- (void)dealloc
{
    [_collectLabel release];
    [_collectImage release];
    [super dealloc];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.collectImage.frame = CGRectMake(10, 10, self.contentView.frame.size.width / 4 + 10, self.contentView.frame.size.height - 20);
    self.collectLabel.frame = CGRectMake(self.collectImage.frame.size.width + 20, 10, self.contentView.frame.size.width / 4 * 3 - 40, self.contentView.frame.size.height - 20);
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
