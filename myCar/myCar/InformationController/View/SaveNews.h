//
//  SaveNews.h
//  myCar
//
//  Created by 王子洁 on 14/11/5.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SaveNews : NSObject

@property (nonatomic, retain)NSString *title;

@property (nonatomic, retain)NSString *image;

@property (nonatomic, retain)NSString *url;

@end
