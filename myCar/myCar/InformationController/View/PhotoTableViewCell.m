//
//  PhotoTableViewCell.m
//  myCar
//
//  Created by 王子洁 on 14/10/29.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "PhotoTableViewCell.h"

@implementation PhotoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.photoImage = [[UIImageView alloc] init];
        //self.photoImage.backgroundColor = [UIColor yellowColor];
        [self.contentView addSubview:self.photoImage];
        [_photoImage release];
        
        self.titleLabel = [[UILabel alloc] init];
        [self.photoImage addSubview:self.titleLabel];
        
        self.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        self.titleLabel.shadowColor = [UIColor blackColor];
        self.titleLabel.textColor = [UIColor whiteColor];
        [_titleLabel release];
        
        self.countLabel = [[UILabel alloc] init];
        [self.photoImage addSubview:self.countLabel];
        self.countLabel.textColor = [UIColor whiteColor];
        self.countLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        self.countLabel.shadowColor = [UIColor blackColor];
        
        [_countLabel release];
        
    }
    return self;
}

- (void)dealloc
{
    [_photoImage release];
    [_titleLabel release];
    [_countLabel release];
    [super dealloc];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}





- (void)layoutSubviews
{
    [super layoutSubviews];
    self.photoImage.frame = CGRectMake(5, 5, self.contentView.frame.size.width - 10, self.contentView.frame.size.height - 5);
    self.titleLabel.frame = CGRectMake(10, 150, 250, 20);
    self.countLabel.frame = CGRectMake(270, 150, 35, 20);
}
@end
