//
//  NewsTableViewCell.m
//  myCar
//
//  Created by 王子洁 on 14/10/28.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "NewsTableViewCell.h"


@implementation NewsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.newsImageView = [[UIImageView alloc] init];
        //self.newsImageView.backgroundColor = [UIColor blueColor];
        [self.contentView addSubview:self.newsImageView];
        [_newsImageView release];
        
        self.title = [[UILabel alloc] init];
        //self.title.backgroundColor = [UIColor greenColor];
       self.title.numberOfLines = 0;
        self.title.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.title];
        [_title release];
        
        self.date = [[UILabel alloc] init];
        //self.date.backgroundColor = [UIColor redColor];
        self.date.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.date];
        [_date release];
        
        self.count = [[UILabel alloc] init];
        //self.count.backgroundColor = [UIColor grayColor];
        self.count.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:self.count];
        [_count release];
        
    }
    return self;
}

- (void)dealloc
{
    [_count release];
    [_title release];
    [_newsImageView release];
    [_date release];
    [super dealloc];
}

- (void)setInfo:(Information *)info
{
    if (_info != info) {
        [_info release];
        _info = [info retain];
    }
    self.title.text = self.info.title;
    self.count.text = self.info.count;
    self.date.text = self.info.date;
    
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    self.newsImageView.frame = CGRectMake(10, 20, self.contentView.frame.size.width / 4 + 10, self.contentView.frame.size.height - 40);
    self.title.frame = CGRectMake(self.newsImageView.frame.size.width + 20, 20, self.contentView.frame.size.width / 4 * 3 - 40, 30);
    self.count.frame = CGRectMake(self.newsImageView.frame.size.width + 20, 10 + self.title.frame.size.height + self.title.frame.origin.y, 60, 20);
    self.date.frame = CGRectMake(self.count.frame.origin.x + self.count.frame.size.width + 60, 10 + self.title.frame.size.height + self.title.frame.origin.y, 80, 20);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
