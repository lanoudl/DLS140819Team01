//
//  AlbumCollectionViewCell.m
//  myCar
//
//  Created by 王子洁 on 14/10/31.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "AlbumCollectionViewCell.h"

@implementation AlbumCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.myImage = [[UIImageView alloc] init];
        [self.contentView addSubview:self.myImage];
        //self.image.backgroundColor = [UIColor redColor];
        self.myLabel = [[UILabel alloc] init];
        self.myLabel.font = [UIFont systemFontOfSize:14];
        self.myLabel.textColor = [UIColor whiteColor];
        self.myLabel.numberOfLines = 0;
      
        [self.myLabel setBackgroundColor:[UIColor blackColor]];
        
        self.myScrollView = [[UIScrollView alloc] init];
       // self.myScrollView.backgroundColor = [UIColor redColor];
        
        [self.myScrollView addSubview:self.myLabel];
        [self.contentView addSubview:self.myScrollView];

        [_myScrollView release];
        [_myImage release];
        [_myLabel release];
        
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
    [_myScrollView release];
    [_myLabel release];
    [_myImage release];
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    self.myImage.frame = CGRectMake(0, 0, self.contentView.frame.size.width, 220);
    self.myLabel.frame = CGRectMake(10, 10, self.bounds.size.width - 20, self.bounds.size.height);
    [self.myLabel sizeToFit];
//    self.myScrollView.frame = CGRectMake(0, 180, self.bounds.size.width, self.myLabel.frame.size.height + 20);
    self.myScrollView.frame = CGRectMake(0, 250, self.bounds.size.width, 150);
    self.myScrollView.contentSize = CGSizeMake(0, self.myLabel.frame.size.height + 20);
    NSLog(@"%@", self.myLabel);
    NSLog(@"%@", self.myScrollView);
}




@end
