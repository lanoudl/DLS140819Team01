//
//  NewsTableViewCell.h
//  myCar
//
//  Created by 王子洁 on 14/10/28.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Information.h"

@interface NewsTableViewCell : UITableViewCell

@property (nonatomic, retain)UIImageView *newsImageView;
@property (nonatomic, retain)UILabel *title;
@property (nonatomic, retain)UILabel *count;
@property (nonatomic, retain)UILabel *date;
@property (nonatomic, retain)Information *info;

@end
