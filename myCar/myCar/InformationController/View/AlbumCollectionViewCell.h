//
//  AlbumCollectionViewCell.h
//  myCar
//
//  Created by 王子洁 on 14/10/31.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Information.h"
@interface AlbumCollectionViewCell : UICollectionViewCell

@property (nonatomic, retain)UIImageView *myImage;
@property (nonatomic, retain)UILabel *myLabel;


@property (nonatomic, retain)Information *info;

@property (nonatomic, retain)UIScrollView *myScrollView;

@end
