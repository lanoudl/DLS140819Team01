//
//  CollectTableViewCell.h
//  myCar
//
//  Created by 王子洁 on 14/11/6.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectTableViewCell : UITableViewCell

@property (nonatomic, retain)UIImageView *collectImage;
@property (nonatomic, retain)UILabel *collectLabel;

@end
