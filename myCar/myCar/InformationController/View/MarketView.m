//
//  MarketView.m
//  myCar
//
//  Created by 王子洁 on 14/11/1.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "MarketView.h"

#import "WebColor.h"

@implementation MarketView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.myLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 50, 30)];
        self.myLabel.backgroundColor = [WebColor whiteSmoke];
        self.myLabel.text = @"地区";
        self.myLabel.font = [UIFont systemFontOfSize:17];
        //self.myLabel.tintColor = [WebColor whiteSmoke];
        self.myLabel.textAlignment = 1;
        [self addSubview:self.myLabel];
        [_myLabel release];
        
        self.myButton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.myButton.frame = CGRectMake(200, 10, 110, 30);
        self.myButton.backgroundColor = [WebColor white];

        [self addSubview:self.myButton];
        
        
    }
    return self;
}

- (void)dealloc
{
    [_myButton release];
    [_myLabel release];
    [super dealloc];
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
