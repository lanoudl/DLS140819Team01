//
//  MarketView.h
//  myCar
//
//  Created by 王子洁 on 14/11/1.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MKReverseGeocoder;
@class CLLocationManager;
@interface MarketView : UIView


@property (nonatomic, retain) UILabel *myLabel;

@property (nonatomic, retain) UIButton *myButton;


@end
