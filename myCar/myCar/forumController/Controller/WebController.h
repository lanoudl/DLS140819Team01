//
//  WebController.h
//  myCar
//
//  Created by 刘莉 on 14-11-7.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Forum.h"
#import "SaveNews.h"
@interface WebController : UIViewController
@property (nonatomic, retain) Forum *forum;
@property (nonatomic, retain)SaveNews *news;
@end
