
//
//  WebController.m
//  myCar
//
//  Created by 刘莉 on 14-11-7.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "WebController.h"

#import "SaveNews.h"
#import "DataBaseHandler.h"
#import <Comment/Comment.h>
#import "WebColor.h"
#import "MBProgressHUD.h"

@interface WebController ()<UIGestureRecognizerDelegate, UIWebViewDelegate>

@property (nonatomic, retain)UIButton *collectButton;

@property (nonatomic, retain)DataBaseHandler *handler;

@property (nonatomic, retain)MBProgressHUD *hub;

@end

@implementation WebController

- (void)dealloc
{
    [super dealloc];
    [_news release];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.news = [[SaveNews alloc] init];
        //self.car = [[TrendCar alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    // Do any additional setup after loading the view.

    [self setWebView];
    
    self.handler = [DataBaseHandler shareInstance];
    [self.handler openDB];
    [self.handler createForumTable];
    [self setButton];
    [self setNavItem];
    [self.navigationController.navigationBar addSubview:self.collectButton];
    // 菊花转起来
    [self setHub];
    
}

- (void)setHub
{
    self.hub = [[MBProgressHUD alloc] initWithView:self.view];
    [self.hub setMode:MBProgressHUDModeIndeterminate];
    [self.hub setFrame:CGRectMake(0, 0, 320, 480)];
    [self.hub setMinSize:CGSizeMake(100, 100)];
    [self.view addSubview:self.hub];
    [self.hub show:YES];
}

- (void)setWebView
{
    NSString *url = [NSString stringWithFormat:@"http://mrobot.pcauto.com.cn/v2/bbs/topics/%@?pageNo=1&pageSize=19&picRule=2&authorId=0&topicTemplate=4.0.0&app=pcautobrowser&size=18", self.forum.topicId];
    UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 60)];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    webview.delegate = self;
    webview.scrollView.bounces = NO;
    [self.view addSubview:webview];
    [webview loadRequest:request];
    [webview release];
}

- (void)setNavItem
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"share.png"] style:UIBarButtonItemStylePlain target:self action:@selector(share:)];
    self.navigationItem.rightBarButtonItem.tintColor = [WebColor white];
}

- (void)setButton
{
    self.collectButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.collectButton.frame = CGRectMake(self.view.frame.size.width - 100, 8, 30, 25);
    [self.collectButton setImage:[UIImage imageNamed:@"collect1.png"] forState:UIControlStateNormal];
    
    [self.collectButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    NSMutableArray *arr = [self.handler selectAllForum];
    for (SaveNews *news in arr) {
        // NSLog(@"%d.....", a);
        NSString *str = [NSString stringWithFormat:@"%@",self.forum.topicId];
        if ([news.url isEqualToString:str]) {
            [self.collectButton setImage:[UIImage imageNamed:@"collect2.png"] forState:UIControlStateNormal];
            break;
        } else {
            [self.collectButton setImage:[UIImage imageNamed:@"collect1.png"] forState:UIControlStateNormal];
            //NSLog(@"-------");
        }
        
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.hub removeFromSuperview];
}

- (void)buttonClicked:(UIButton *)button
{
    SaveNews *news = [[SaveNews alloc]init];
    news.url = self.forum.topicId;
    news.title = self.forum.title;
    if (button.currentImage == nil || ([button.currentImage isEqual:[UIImage imageNamed:@"collect1.png"]])) {
        [button setImage:[UIImage imageNamed:@"collect2.png"] forState:UIControlStateNormal];
        [self.handler insertForum:news];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"收藏成功" delegate:self cancelButtonTitle:@"请返回" otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
        [news release];
    } else {
        [button setImage:[UIImage imageNamed:@"collect1.png"] forState:UIControlStateNormal];
       
        [self.handler deleteForum:news];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"删除成功" delegate:self cancelButtonTitle:@"请返回" otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
    }
}

- (void)share:(UIBarButtonItem *)button
{
    id<ISSContent> content = [ShareSDK content: self.forum.title defaultContent:@"要分享的内容为空时" image:nil title:@"share" url:@"http://www.baidu.com" description:@"这是描述" mediaType:SSPublishContentMediaTypeNews];
    
    [ShareSDK showShareActionSheet:nil shareList:nil content:content statusBarTips:YES authOptions:nil shareOptions:nil result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享" message:nil delegate:self cancelButtonTitle:@"返回" otherButtonTitles:nil, nil];
        [self.view addSubview:alertView];
        if (state == SSResponseStateSuccess) {
            NSLog(@"分享成功");
            [alertView setTitle:@"分享成功"];
            [alertView show];
        } else if (state == SSResponseStateFail) {
            NSLog(@"分享失败");
            [alertView setTitle:@"分享失败"];
            [alertView show];
        }
    }];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    self.collectButton.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.collectButton.hidden = YES;
    
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
