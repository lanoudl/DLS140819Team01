//
//  Forum.h
//  myCar
//
//  Created by 刘莉 on 14-11-7.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Forum : NSObject

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *view;
@property (nonatomic, retain) NSString *replyCount;
@property (nonatomic, retain) NSString *topicId;
@property (nonatomic, retain) NSString *flag;
@property (nonatomic, retain) NSString *nickName;
- (id)initWithDictionary:(NSDictionary *)dic;
       
@end
