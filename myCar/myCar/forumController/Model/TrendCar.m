//
//  TrendCar.m
//  myCar
//
//  Created by 吴东升 on 14/10/28.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "TrendCar.h"

@implementation TrendCar

- (id)initWithDictionary:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        self.imageStr = [dic objectForKey:@"image"];
        self.titleStr = [dic objectForKey:@"title"];
        self.forumStr = [dic objectForKey:@"forumName"];
        self.countStr = [dic objectForKey:@"replyCount"];
        self.IdStr = [dic objectForKey:@"topicId"];
    }
    return self;
}

- (void)dealloc
{
    [_imageStr release];
    [_titleStr release];
    [_forumStr release];
    [_countStr release];
    [_IdStr release];
    
    [super dealloc];
}

@end
