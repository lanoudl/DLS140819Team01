//
//  Forum.m
//  myCar
//
//  Created by 刘莉 on 14-11-7.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "Forum.h"

@implementation Forum

- (void)dealloc
{
    [_flag       release];
    [_title      release];
    [_view       release];
    [_nickName   release];
    [_replyCount release];
    [super       dealloc];
}
- (id)initWithDictionary:(NSDictionary *)dic
{
    if (self = [super init]) {
        self.title = [dic objectForKey:@"title"];
        self.view = [dic objectForKey:@"view"];
        self.replyCount = [dic objectForKey:@"replyCount"];
        if ([[dic allKeys] containsObject:@"flag"]) {
            self.flag = [dic objectForKey:@"flag"];
        } else {
            self.flag = @"nil";
        }
        self.topicId = [dic objectForKey:@"topicId"];

    }
    
    NSDictionary *authorDic = [dic objectForKey:@"author"];
    self.nickName = [authorDic objectForKey:@"nickname"];

    return self;
}

@end
