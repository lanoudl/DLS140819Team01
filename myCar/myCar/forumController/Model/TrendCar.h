//
//  TrendCar.h
//  myCar
//
//  Created by 吴东升 on 14/10/28.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrendCar : NSObject

@property (nonatomic, retain)NSString *imageStr;
@property (nonatomic, retain)NSString *titleStr;
@property (nonatomic, retain)NSString *forumStr;
@property (nonatomic, retain)NSString *countStr;
@property (nonatomic, retain)NSString *IdStr;

- (id)initWithDictionary:(NSDictionary *)dic;



@end
