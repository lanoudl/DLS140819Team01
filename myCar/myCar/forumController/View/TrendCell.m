//
//  TrendCell.m
//  myCar
//
//  Created by 吴东升 on 14/10/28.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "TrendCell.h"

@implementation TrendCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.trendImage = [[UIImageView alloc] init];
        [self.contentView addSubview:self.trendImage];
        [_trendImage release];
        
        self.titLabel = [[UILabel alloc] init];
        self.titLabel.font = [UIFont systemFontOfSize:15];
        self.titLabel.numberOfLines = 0;
        [self.contentView addSubview:self.titLabel];
        [_titLabel release];
        
        self.forumLabel = [[UILabel alloc] init];
        self.forumLabel.font = [UIFont systemFontOfSize:13];
        self.forumLabel.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:self.forumLabel];
        [_forumLabel release];
        
        self.countLabel = [[UILabel alloc] init];
        self.countLabel.font = [UIFont systemFontOfSize:12];
        self.countLabel.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:self.countLabel];
        [_countLabel release];
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.trendImage.frame = CGRectMake(5, 1, self.contentView.frame.size.width / 3, 58);
    
    self.titLabel.frame = CGRectMake(self.contentView.frame.size.width / 3 + 15, 5, self.contentView.frame.size.width / 3 * 2 - 25, 40);
    
    self.countLabel.frame = CGRectMake(self.contentView.frame.size.width / 3 + 15, 55, self.contentView.frame.size.width / 5 , 20);
    
    self.forumLabel.frame = CGRectMake(self.contentView.frame.size.width / 3 * 2, 55, self.contentView.frame.size.width / 7  * 2, 20);
}

- (void)dealloc
{                                                      
    [_trendImage release];
    [_titLabel release];
    [_forumLabel release];
    [_countLabel release];
    
    [super dealloc];
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
