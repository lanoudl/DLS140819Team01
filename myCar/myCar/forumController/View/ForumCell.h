//
//  ForumCell.h
//  myCar
//
//  Created by 刘莉 on 14-11-7.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForumCell : UITableViewCell
@property (nonatomic, retain)UILabel *flagLabel;
@property (nonatomic, retain)UILabel *titLabel;
@property (nonatomic, retain)UILabel *nameLabel;
@property (nonatomic, retain)UILabel *replyLabel;
@end
