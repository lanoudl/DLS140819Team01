//
//  ForumCell.m
//  myCar
//
//  Created by 刘莉 on 14-11-7.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "ForumCell.h"

@implementation ForumCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.flagLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.flagLabel];
        
        self.titLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.titLabel];
        
        self.nameLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.nameLabel];
        
        self.replyLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.replyLabel];
        self.flagLabel.font = [UIFont systemFontOfSize:14];
        self.flagLabel.textAlignment = NSTextAlignmentCenter;
        self.flagLabel.textColor = [UIColor whiteColor];
        self.titLabel.font = [UIFont systemFontOfSize:16];
        self.titLabel.numberOfLines = 0;
        self.nameLabel.font = [UIFont systemFontOfSize:14];
        self.nameLabel.textColor = [UIColor lightGrayColor];
        self.replyLabel.font = [UIFont systemFontOfSize:14];
        self.replyLabel.textColor = [UIColor lightGrayColor];
    }
    return self;
}

- (void)dealloc
{
    [_flagLabel release];
    [_titLabel release];
    [_nameLabel release];
    [_replyLabel release];
    [super dealloc];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat width = self.contentView.frame.size.width;
    CGFloat height = self.contentView.frame.size.height;
    
    self.flagLabel.frame = CGRectMake(10, 10, 20, height / 4);
    
    self.titLabel.frame = CGRectMake(35, 0, width - 35, height / 4 * 3);
    self.nameLabel.frame = CGRectMake(30, height / 4 * 3  + 5, 150, height / 4 - 5);
    self.replyLabel.frame = CGRectMake( 190, height / 4 * 3 + 5, 150, height / 4 - 5);
   
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
