//
//  TrendCell.h
//  myCar
//
//  Created by 吴东升 on 14/10/28.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrendCell : UITableViewCell


@property (nonatomic, retain)UIImageView *trendImage;
@property (nonatomic, retain)UILabel *titLabel;
@property (nonatomic, retain)UILabel *forumLabel;
@property (nonatomic, retain)UILabel *countLabel;

@end
