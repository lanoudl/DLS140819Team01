//
//  Car.h
//  myCar
//
//  Created by 刘莉 on 14-10-27.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Car : NSObject
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *logo;
@property (nonatomic, retain) NSNumber *idNum;

- (id)initWithDictionary:(NSDictionary *)dic;

- (id)initWithArray:(NSArray *)array;

@end
