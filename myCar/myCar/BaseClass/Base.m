//
//  Base.m
//  myCar
//
//  Created by 刘莉 on 14-11-6.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "Base.h"

@implementation Base
-(void)dealloc
{
    [_idNum release];
    [_name  release];
    
    [super  dealloc];
}

- (id)initWithArray:(NSArray *)array
{
    self = [super init];
    if (self) {
        self.idNum = array[0];
        self.name = array[1];
        
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
    
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.idNum = value;
    }
}

- (id)valueForUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        return self.idNum;
    }
    return nil;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.idNum = [aDecoder decodeObjectForKey:@"idNum"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
        
        
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    //  编码的方法，将对象的所有数据存在一起,方便存储
    
    // 将某一条数据编码
    [aCoder encodeObject :self.idNum   forKey:@"idNum"];
    [aCoder encodeObject :self.name    forKey:@"name"];
    
    
}

@end
