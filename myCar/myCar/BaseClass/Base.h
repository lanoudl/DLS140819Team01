//
//  Base.h
//  myCar
//
//  Created by 刘莉 on 14-11-6.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Base : NSObject<NSCoding>
@property (nonatomic, retain) NSString *name;

@property (nonatomic, retain) NSString *idNum;

- (id)initWithDictionary:(NSDictionary *)dic;

- (id)initWithArray:(NSArray *)array;
@end
