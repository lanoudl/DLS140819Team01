//
//  Car.m
//  myCar
//
//  Created by 刘莉 on 14-10-27.
//  Copyright (c) 2014年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "Car.h"

@implementation Car
-(void)dealloc
{
    [_idNum release];
    [_name  release];
    [_logo release];
    [super  dealloc];
}

- (id)initWithArray:(NSArray *)array
{
    self = [super init];
    if (self) {
        self.idNum = array[0];
        self.name = [array[1] substringFromIndex:1];
        self.logo = array[2];
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
    
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.idNum = value;
    }
}

- (id)valueForUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        return self.idNum;
    }
    return nil;
}
@end
